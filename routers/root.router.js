const { userRouter } = require("./user.router");
const express = require("express");
const { authRouter } = require("./auth.router");
const { cinemaRouter } = require("./cinema.router");
const rootRouter = express.Router();

rootRouter.use("/users", userRouter);
rootRouter.use("/auth", authRouter);
rootRouter.use("/cinemas", cinemaRouter);

// rootRouter.use("/movies", movieRouter);

module.exports = {
  rootRouter,
};
