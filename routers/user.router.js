const express = require("express");
const userRouter = express.Router();
const { User } = require("../models");

// cai sever database
/**
 * yarn add sequelize
 * yarn add mysql2
 * yarn add sequelize-cli --dev
 * npx sequelize --help
 * yarn add multer (--upload file)
 * yarn add mkdirp (tạo đường dẫn)
 */

const {
  getListUser,
  getDetailUser,
  getCreateUser,
  getRemoveUser,
  getUpdateUser,
  uploadAvatar,
} = require("../controllers/user.controller");
const { logger } = require("../middlewares/log/logger.middleware");
const {
  checkExist,
} = require("../middlewares/validations/checkExits.middlewares");
const {
  authenticate,
  authorize,
} = require("../middlewares/auth/verify-token.middleware");
const {
  uploadImageSingle,
} = require("../middlewares/uploads/upload-image.middleware");

// xây dựng api quản lý người dùng

/**
 * api : lấy tất cả người dùng
 *    url : '/api/users/get-list-user'
 *    method : get
 */
userRouter.post(
  "/upload-avatar",
  authenticate,
  uploadImageSingle("avatar"),
  uploadAvatar
);

userRouter.get("/", logger("Lấy tất cả người dùng"), getListUser);
/**
 * api : lấy chi tiết một người dùng
 *    url : '/api/users/get-detail-user/:id'
 *    method : get
 */

userRouter.get(
  "/:id",
  logger("Lấy tất cả thông tin người dùng"),
  getDetailUser
);
/**
 * api : tạo mới user
 *    url : '/api/users/create-user'
 *    method : post,
 *    data : { email , name }
 */
userRouter.post("/", authenticate, authorize(["admin"]), getCreateUser);

userRouter.delete("/:id", checkExist(User), getRemoveUser);

userRouter.put("/:id", logger("Update người dùng"), getUpdateUser);

module.exports = {
  userRouter,
};
