const express = require("express");
const { signIn, SignUp } = require("../controllers/auth.controller");
const authRouter = express.Router();

authRouter.post("/sign-in", signIn);
authRouter.post("/sign-up", SignUp);

module.exports = {
  authRouter,
};
