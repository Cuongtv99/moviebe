const express = require("express");
const path = require("path");
const app = express();
const cors = require("cors");

// set up cors
app.use(cors());

const { rootRouter } = require("./routers/root.router");
// set up Graphql
const { graphqlHTTP } = require("express-graphql");
const { graphqlSchema } = require("./graphql/schema");
const { graphqlResolver } = require("./graphql/resolvers");
app.use(
  "/graphql",
  graphqlHTTP({
    schema: graphqlSchema, // để schema vào
    rootValue: graphqlResolver, // để resolve vào
    graphiql: true,
  })
);
// setup app sử dụng data dạng json
app.use(express.json());

// set up static file để truy cập được các file sever
const publicPathDirectory = path.join(__dirname, "./public");
app.use("/public", express.static(publicPathDirectory));

/**
 * method : get
 * url : "/" <==> http://localhost:7000/
 */
const port = 7000;
app.get("/", (req, res) => {
  console.log("object");
});
app.listen(port, () => {
  console.log(`app run on port ${port}`);
});
app.use("/api", rootRouter);
