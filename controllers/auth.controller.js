// yarn add bcryptjs
// yarn add jsonwebtoken

const { User } = require("../models");
const bcryptjs = require("bcryptjs");
const jwt = require("jsonwebtoken");

const signIn = async (req, res) => {
  const { email, password } = req.body;
  /**
   * 2 bước đăng nhập
   *    1/ tìm user theo email
   *    2/so sánh password
   */
  try {
    const userLogin = await User.findOne({ where: { email } });
    if (userLogin) {
      // So sánh password
      const isAuth = bcryptjs.compareSync(password, userLogin.password);
      if (isAuth) {
        const payload = {
          id: userLogin.id,
          email: userLogin.email,
          role: userLogin.role,
        };
        const secretKey = "cuongTV";
        const token = jwt.sign(payload, secretKey, {
          expiresIn: 365 * 24 * 60 * 60,
        });
        res.status(200).send({ message: "Dang nhap thanh cong", token });
      } else {
        res.status(400).send("Wrong password");
      }
    } else {
      res.status(404).send("Not Found Email");
    }
  } catch (error) {
    res.status(500).send(error);
  }
};

const SignUp = async (req, res) => {
  const { name, email, password, phone, age } = req.body;
  try {
    // tạo ra 1 chuỗi ngẫu nhiên
    const salt = bcryptjs.genSaltSync(10);
    // mã hóa password
    const hashPassword = bcryptjs.hashSync(password, salt);
    const newUser = await User.create({
      name,
      email,
      password: hashPassword,
      phone,
      age,
      role: "CLIENT",
    });
    res.status(201).send(newUser);
  } catch (error) {
    res.status(500).send(error);
  }
};

module.exports = {
  signIn,
  SignUp,
};
