"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    await queryInterface.bulkInsert(
      "Cinemas",
      [
        {
          id: "1",
          name: "BHD 3/2",
          image: "link hình",
          address: "Quận 10 HCM",
          cineplexId: "1",
          createdAt: "2021-06-25 14:15:23",
          updatedAt: "2021-06-25 14:15:23",
        },
        {
          id: "2",
          name: "CGV Thủ Đức",
          image: "link hình",
          address: "Quận Thủ Đức HCM",
          cineplexId: "2",
          createdAt: "2021-06-25 14:15:23",
          updatedAt: "2021-06-25 14:15:23",
        },
        {
          id: "3",
          image: "link hình",
          address: "Quận 1 HCM",
          name: "Lotte Trường Chinh",
          cineplexId: "3",
          createdAt: "2021-06-25 14:15:23",
          updatedAt: "2021-06-25 14:15:23",
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete("Cinemas", null, {});
  },
};
