"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    await queryInterface.bulkInsert(
      "Cineplexes",
      [
        {
          id: 1,
          name: "BHD",
          logo: "link hinh BHD",
          createdAt: "2021-06-25 14:15:23",
          updatedAt: "2021-06-25 14:15:23",
        },
        {
          id: 2,
          name: "CGV",
          logo: "link hinh CGV",
          createdAt: "2021-06-25 14:15:23",
          updatedAt: "2021-06-25 14:15:23",
        },
        {
          id: 3,
          name: "Lotte",
          logo: "link hinh Lotte",
          createdAt: "2021-06-25 14:15:23",
          updatedAt: "2021-06-25 14:15:23",
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete("Cineplexes", null, {});
  },
};
