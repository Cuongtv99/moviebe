const { User } = require("../models");
const graphqlResolver = {
  async userDetail({ id }) {
    try {
      const user = await User.findByPk(id);
      return user;
    } catch (error) {
      throw new Error(error);
    }
    // return {
    //   id: 1,
    //   name: "Nguyen Phong Hao",
    //   email: "hao@gmail.com",
    //   password: "123456",
    //   phone: "0125478963",
    //   age: 19,
    //   role: "ADMIN",
    //   avatar: "caigido.png",
    // };
  },
  async usersList() {
    try {
      const users = await User.findAll();
      return users;
    } catch (error) {
      throw new Error(error);
    }
  },
  async createUser({ inputUser }) {
    try {
      const user = await User.create({ ...inputUser });
      return user;
    } catch (error) {}
  },
  async removeUser({ id }) {
    try {
      const user = await User.findByPk(id);
      if (user) {
        User.destroy({
          where: {
            id,
          },
        });
        return user;
      } else {
        throw new Error("Not Found");
      }
    } catch (error) {
      throw new Error(error);
    }
  },
};

module.exports = {
  graphqlResolver,
};
