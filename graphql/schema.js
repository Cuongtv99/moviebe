const { buildSchema } = require("graphql");
const graphqlSchema = buildSchema(`
    type User{
        id: Int!
        name: String!
        email: String!
        password: String!
        phone: String!
        age: Int!
        role: String!
        avatar: String!
    }
    type rootQuery{
        userDetail(id:Int): User!
        usersList : [User]! 
    }

    input InputUser{
        name: String!
        email: String!
        password: String!
        phone: String!
        age: Int!
        role: String!
        avatar: String!
    }

    type rootMutation{
        createUser(inputUser :InputUser ) : User!
        removeUser(id: Int!) : User!
    }

    schema{
        query: rootQuery
        mutation: rootMutation 
    }
`);

module.exports = {
  graphqlSchema,
};
