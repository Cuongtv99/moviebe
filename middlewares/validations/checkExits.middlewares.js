const checkExist = (model) => async (req, res, next) => {
  const { id } = req.params;
  const detail = await model.findByPk(id);
  if (detail) {
    next();
  } else {
    res.status(404).send("Not Found");
  }
};

module.exports = {
  checkExist,
};
