const logger = (message) => (req, res, next) => {
  console.log(message);
  // res
  //   .status(400)
  //   .send("Không chạy tiếp middleware, mà phản ứng về cho client");
  next(); // chạy tiếp tục tới middleware khác
};
module.exports = {
  logger,
};
