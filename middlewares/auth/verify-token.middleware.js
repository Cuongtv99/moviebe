const jwt = require("jsonwebtoken");

const authenticate = (req, res, next) => {
  const token = req.header("token");
  try {
    const secretKey = "cuongTV";
    var decode = jwt.verify(token, secretKey);
    req.tokenDecode = decode;
    next();
  } catch (err) {
    // err
    res.status(401).send("Ban chua dang nhap");
  }
};
// phân quyền
/**
 *
 * userTypeArray  - ["admin","client"]
 *  1. "admin"  → "admin"=userTypeArray ==>next()
 */
const authorize = (userTypeArray) => {
  return (req, res, next) => {
    const { tokenDecode } = req;
    const dk =
      userTypeArray.findIndex((type) => {
        return type === tokenDecode.role;
      }) > -1;
    if (dk) {
      return next();
    } else {
      res.status(403).send({
        message: "Bạn đã đang nhập nhưng không có đủ quyền để thao tác",
      });
    }
  };
};

module.exports = {
  authenticate,
  authorize,
};
